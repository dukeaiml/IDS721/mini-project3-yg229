// Use AWS CDK to create an S3 bucket with versioning and encryption enabled.
import * as cdk from 'aws-cdk-lib';
import { Construct } from 'constructs';
import { aws_s3 as s3 } from 'aws-cdk-lib';
export class  Mini3BucketStack extends cdk.Stack {
  constructor(scope: Construct, id: string, props?: cdk.StackProps) {
    super(scope, id, props);
    const bucket = new s3.Bucket(this, 'mini3-bucket', {
      bucketName: 'mini3-bucket',
      versioned: true,
      encryption: s3.BucketEncryption.KMS_MANAGED,
    });
  }
}