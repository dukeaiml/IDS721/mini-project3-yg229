# yg229_mini_project_3

In this project, I create an S3 Bucket named 'mini3-bucket' using CDK with AWS CodeWhisperer.

## Author

Yanbo Guan

## Configuration and Initialization:

1. Download AWS Toolkit
![](1.png)

2. Download node.js
3. Install AWS CDK: ``` npm install -g aws-cdk```
4. Install the S3 Package: ``` npm install @aws-cdk/aws-s3```
5. Initialize a new CDK project with ```cdk init app --language=typescript```
6. Use Code Whispper to Generate S3 Bucket Code: In the file ```mini3-bucket-stack.ts```, write the comment as below
```
// Use AWS CDK to create an S3 bucket with versioning and encryption enabled.
```
7. Press Enter at the end of the comment, tap to confirm the code.
8. Prepare an AWS environment for CDK deployments ```cdk bootstrap aws://533267227404/us-east-1 ```
![](2.png)
9. Deploy the project. ```cdk deploy```
![](3.png)
The new bucket created by CDK is shown on S3 Bucket webpage


